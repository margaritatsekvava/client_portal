<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::group(['prefix'=>'users'],function(){

    Route::post('/create', array('as' => 'Create User', 'uses' => 'UsersController@createUser'));
    Route::post('/{user_id}/update', array('as' => 'Update User', 'uses' => 'UsersController@updateUser'));
    Route::post('/{user_id}/password/update', array('as' => 'Update User Password', 'uses' => 'UsersController@updatePassword'));
    Route::post('/{user_id}/avatar/update', array('as' => 'Update Avatar', 'uses' => 'UsersController@updateAvatar'));

});

Route::group(['prefix'=>'projects'],function(){

    Route::get('/{project_id}', array('as' => 'Get Project Info', 'uses' => 'ProjectsController@readProjectInfo'));
    Route::get('client/{client_id}', array('as' => 'Get Projects info for this client', 'uses' => 'ProjectsController@readClientProjects'));
    Route::get('admin/{admin_id}', array('as' => 'Get Projects info for this admin', 'uses' => 'ProjectsController@readAdminProjects'));
    Route::get('client/{client_id}/latest', array('as' => 'Get latest Project info for this client', 'uses' => 'ProjectsController@readClientLatestProject'));
    Route::get('admin/{admin_id}/latest', array('as' => 'Get latest Project info for this Admin', 'uses' => 'ProjectsController@readAdminLatestProject'));

    Route::post('/createProject', array('as' => 'Create Project', 'uses' => 'ProjectsController@createProject'));
    Route::post('/{project_id}/update', array('as' => 'Create Project', 'uses' => 'ProjectsController@updateProject'));
    Route::post('/{project_id}/feedback/create', array('as' => 'Create Project Feedback', 'uses' => 'ProjectFeedbackController@createProjectFeedback'));
    Route::post('/{project_id}/progress/create', array('as' => 'Create Project Progress', 'uses' => 'ProjectProgressController@createProjectProgress'));
    Route::post('/{project_id}/specs/upload', array('as' => 'Upload Project Spec', 'uses' => 'ProjectSpecsController@specsUpload'));
    Route::post('/{project_id}/progress/complete', array('as' => 'Progress Complete', 'uses' => 'ProjectProgressController@completeProgress'));


});

    Route::post('/login', ['as' => 'login_path','uses' => 'AuthController@login']);
    Route::post('/social/sync/facebook', ['as' => 'social_sync_facebook_path','uses' => 'SocialAuthController@socialSyncFacebook']);
    Route::post('/social/sync/twitter', ['as' => 'social_sync_twitter_path','uses' => 'SocialAuthController@socialSyncTwitter']);
    Route::get('', ['as' => 'login','uses' => 'PagesController@indexView']);
    Route::any('/logout', ['as' => 'logout_path','uses' => 'AuthController@logout']);
    Route::get('/app', ['as' => 'app','uses' => 'AppController@indexView']);
    Route::get('/social/sync/facebook', ['as' => 'social_sync_facebook','uses' => 'PagesController@socialSyncFacebookView']);
    Route::get('/social/sync/twitter', ['as' => 'social_sync_twitter','uses' => 'PagesController@socialSyncTwitterView']);


Route::group(['prefix'=>'password'],function(){
    Route::get('/', ['as' => 'password_reminder','uses' => 'RemindersController@getRemind']);
    Route::post('/', ['as' => 'password_reminder_path','uses' => 'RemindersController@postRemind']);
    Route::get('/reset/{token}', ['uses' => 'RemindersController@getReset']);
    Route::post('/reset', ['as' => 'password_reset_path','uses' => 'RemindersController@postReset']);
});



