<?php namespace techtank\Mailers;
use Config;
use User;
use Project;

/**
 * Class UserMailer
 * @package techtank\Mailers
 */
class UserMailer extends Mailer{


    /**
     * @param $user
     */
    public function sendWelcomeMessageTo($user){

        $subject = "Welcome to Techtank";
        $view = "emails.user.welcome.welcome";
        $data = array(
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'password'=>$user->password,
            'user_id'=>$user->user_id,
            //'api_url'=>Config::get('app.api_url'),
            //'csrf'=>Config::get('auth.csrf')
        );

        return $this->sendTo($user,$subject,$view,$data);

    }

    public function sendNewProjectCreatedTo($user, $project){
        $subject = "New Project Create";
        $view = "emails.user.newProject.newProject";

        $data = array(
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'project_name' => $project->name,

        );

        return $this->sendTo($user,$subject,$view,$data);

    }



    public function sendThankForFeedbackTo($user,$project ){


        $subject = "Thank for Feedback";
        $view = "emails.user.thankForFeedback.thankForFeedback";
        $data = array(
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'password' => $user->password,
            'project_name'=>$project->name,


        );

        return $this->sendTo($user,$subject,$view,$data);

    }

    public function sendProgressCreatedTo($user,$progress,$project ){


        $subject = "Progress Created";
        $view = "emails.user.progressCreated.progressCreated";
        $data = array(
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'password' => $user->password,
            'project_name'=>$project->name,
            'percentage' => $progress->percentage,
            'comment' => $progress->comment,

        );

        return $this->sendTo($user,$subject,$view,$data);

    }

    public function sendProjectCompletedTo($user,$project ){


        $subject = "Progress Completed";
        $view = "emails.user.projectCompleted.projectCompleted";
        $data = array(
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'password' => $user->password,
            'project_name'=>$project->name,

        );

        return $this->sendTo($user,$subject,$view,$data);

    }

}