<?php namespace techtank\Mailers;
use Config;
use User;
use project;

/**
 * Class AdminMailer
 * @package techtank\Mailers
 */
class AdminMailer extends Mailer{

    public function sendSpecUploadedMessageTo($admin, $project){


        $subject = "Project Spec Uploaded";
        $view = "emails.admin.projectSpecUploaded.projectSpecUploaded";
        $data = array(
            'first_name' => $admin->first_name,
            'last_name' => $admin->last_name,
            'email' => $admin->email,
            'password'=>$admin->password,
            'project_name'=>$project->project_name

        );

        return $this->sendTo($admin,$subject,$view,$data);

    }

    public function sendFeedbackCreatedTo($feedback,$project, $admin ){


        $subject = "Feedback Created";
        $view = "emails.admin.feedbackCreated.feedbackCreated";
        $data = array(
            'first_name' => $admin->first_name,
            'last_name' => $admin->last_name,
            'email' => $admin->email,
            'password'=>$admin->password,
            'project_name'=>$project->name,
            'project_rating' => $feedback->rating,
            'project_quotation' => $feedback->quotation

        );

        return $this->sendTo($admin,$subject,$view,$data);

    }

}