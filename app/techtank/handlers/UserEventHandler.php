<?php

namespace techtank\Handlers;
use techtank\Mailers\UserMailer as UserMailer;
//use uniwink\Mailers\AdminMailer as AdminMailer;
use User;
use Project;


/**
 * Class UserEventHandler
 * @package techtank\Handlers
 */
class UserEventHandler {

    /**
     * @var \techtank\Mailers\UserMailer
     */
    private $mail;
    /**
     * @param Mailer $mail
     */

    function __construct(UserMailer $userMailer){

        $this->userMailer = $userMailer;

    }

    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen('userCreated', 'techtank\Handlers\UserEventHandler@handleNewUser');
        $events->listen('projectCreated', 'techtank\Handlers\UserEventHandler@handleNewProject');
        $events->listen('projectCompleted', 'techtank\Handlers\UserEventHandler@handleProjectCompleted');
        $events->listen('thankForFeedback', 'techtank\Handlers\UserEventHandler@handleThankForFeedback');
        $events->listen('progressCreated', 'techtank\Handlers\UserEventHandler@handleNewProgressCreated');
    }

    public function handleNewUser($user){
        //SegmentEventHandler::handleNewSignUpEvent($user);
        //dd("New User is Created") ;
        $this->userMailer->sendWelcomeMessageTo($user);

    }

    public function handleNewProject($user, $project){
        //$user = User::find($project->client_id);

      $this->userMailer->sendNewProjectCreatedTo($user, $project);
    }

    public function handleProjectCompleted($user, $project){
        //$user = User::find($project->client_id);

        $this->userMailer->sendProjectCompletedTo($user,$project);
    }

    public function handleNewProgressCreated($user, $progress,$project){
        //$user = User::find($project->client_id);

        $this->userMailer->sendProgressCreatedTo($user, $progress,$project);
    }

    public function handleThankForFeedback($user, $project){
        //$user = User::find($project->client_id);

        $this->userMailer->sendThankForFeedbackTo($user, $project);
    }



}

