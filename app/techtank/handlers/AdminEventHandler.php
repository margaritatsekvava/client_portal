<?php

namespace techtank\Handlers;

use techtank\Mailers\AdminMailer as AdminMailer;
use User;
use Project;


/**
 * Class AdminEventHandler
 * @package techtank\Handlers
 */
class AdminEventHandler {

    /**
     * @var \techtank\Mailers\AdminMailer
     */
    private $mail;
    /**
     * @param Mailer $mail
     */

    function __construct(AdminMailer $adminMailer){

        $this->adminMailer = $adminMailer;

    }

    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen('projectSpecUploaded', 'techtank\Handlers\AdminEventHandler@handleSpecUploaded');
        $events->listen('feedbackCreated', 'techtank\Handlers\AdminEventHandler@handleFeedbackCreated');

    }

    public function handleSpecUploaded($admin, $project){

        $this->adminMailer->sendSpecUploadedMessageTo($admin,$project);

    }

    public function handleFeedbackCreated($feedback,$project,$admin){
        //$user = User::find($project->client_id);

        $this->adminMailer->sendFeedbackCreatedTo($feedback, $project, $admin);
    }
}