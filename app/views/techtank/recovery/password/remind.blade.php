@extends('techtank.layouts.externalmaster')

@section('content')

<!-- BEGIN FORGOT PASSWORD FORM -->

    {{ Form::open(array('route' => 'password_reminder_path', 'class'=>'', 'id'=>'form-password-reminder')) }}

    @include('flash::message')
    <h3>Forget Password ?</h3>
    <p>
        Enter your e-mail address below to reset your password.
    </p>
    <div class="form-group">
        {{ Form::email('email',null,['id'=>'password-email','class'=>'form-control placeholder-no-fix','placeholder'=>'Email','required'=>'required']) }}
    </div>
    <div class="form-actions">
        {{ HTML::decode(HTML::linkRoute('login', '<strong>Back</strong>', array(), array('class'=>'btn btn-default'))) }}

        {{ Form::submit('Reset Password', array('id'=>'reset-password','class'=>'btn btn-success uppercase pull-right')) }}

    </div>

    {{ Form::close() }}

<!-- END FORGOT PASSWORD FORM -->


@stop



