@extends('techtank.layouts.externalmaster')

@section('content')
<!-- BEGIN FORGOT PASSWORD FORM -->
{{ Form::open(array('route' => 'password_reset_path', 'class'=>'', 'id'=>'form-password-reminder')) }}

    @include('flash::message')

{{ Form::hidden('token',$token) }}

    <h3>Forget Password ?</h3>
    <p>
        Enter your e-mail address below to reset your password.
    </p>
    <div class="form-group">
        {{ Form::email('email',null,['id'=>'email','class'=>'form-control placeholder-no-fix','placeholder'=>'Email','required'=>'required']) }}
     </div>

<div class="form-group">
    {{ Form::password('password',['id'=>'password','class'=>'form-control placeholder-no-fix','placeholder'=>'New Password','required'=>'required']) }}

</div>

<div class="form-group">
    {{ Form::password('password_confirmation',['id'=>'password_confirmation','class'=>'form-control placeholder-no-fix','placeholder'=>'New Password Again','required'=>'required']) }}
</div>


<div class="form-actions">
    {{ Form::submit('Submit', array('id'=>'reset-password','class'=>'btn btn-success uppercase pull-right')) }}
<br/><br/>

    </div>

<div class="create-account">
    <p>
        {{ HTML::decode(HTML::linkRoute('password_reminder', '<strong>Forgot Password?</strong>', array(), array('class'=>'uppercase'))) }}
&nbsp;&nbsp;&nbsp;
        {{ HTML::decode(HTML::linkRoute('login', '<strong>Login</strong>', array(), array('class'=>'uppercase'))) }}

    </p>
</div>

{{ Form::close() }}
<!-- END FORGOT PASSWORD FORM -->

@stop

