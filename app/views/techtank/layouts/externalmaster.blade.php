/*<?php
$environment = Config::get('app.environment');
$apiUrl = Config::get('app.api_url');
$front_end_url = Config::get('app.front_end_url');
$csrfToken = Config::get('app.csrf_token');
$localPath = Config::get('app.local_path');
$bucket = Config::get('app.bucket');
$version = Config::get('app.version');
$cssUrl = Config::get('app.css_url');
$jsUrl = Config::get('app.js_url');
$jsLibUrl = Config::get('app.js_lib_url');
$jsPluginUrl = Config::get('app.js_plugin_url');
$jsDirectiveUrl = Config::get('app.js_directive_url');
$jsFactoryUrl = Config::get('app.js_factory_url');
$jsControllerUrl = Config::get('app.js_controller_url');
$stripePublishableKey = Config::get('services.stripe.public');
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>TechTank | Client Portal</title>
    <meta content="Portal for TechTanks clients" name="description" />

   <!-- <link rel="shortcut icon"   href="{{{ asset('img/fav-icons/favicon.ico') }}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{{ asset('img/fav-icons/apple-touch-icon.png') }}}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{{ asset('img/fav-icons/apple-touch-icon-57x57.png') }}}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{{ asset('img/fav-icons/apple-touch-icon-72x72.png') }}}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{{ asset('img/fav-icons/apple-touch-icon-76x76.png') }}}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{{ asset('img/fav-icons/apple-touch-icon-114x114.png') }}}" />
    <link rel="apple-touch-icon" sizes="120x120"href="{{{ asset('img/fav-icons/apple-touch-icon-120x120.png') }}}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{{ asset('img/fav-icons/apple-touch-icon-144x144.png') }}}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{{ asset('img/fav-icons/apple-touch-icon-152x152.png') }}}" />-->


    @include('techtank.includes.css')

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a>
        <img src="{{{asset('images/techtank-logo.png')}}}" alt="TechTank" class="logo-default"/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    @yield('content')
</div>

<div class="copyright">
    Copyright © <?php echo date("Y"); ?> <a href="https://www.techtank.eu" target="_blank">TECHTANK.eu</a>
</div>

@include('techtank.includes.external-js')

</body>
<!-- END BODY -->
</html>