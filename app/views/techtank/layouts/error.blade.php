<?php
$environment = Config::get('app.environment');
$apiUrl = Config::get('app.api_url');
$frontEndUrl = Config::get('app.front_end_url');
$csrfToken = Config::get('app.csrf_token');
$localPath = Config::get('app.local_path');
$bucket = Config::get('app.bucket');
$version = Config::get('app.version');
$cssUrl = Config::get('app.css_url');
$jsUrl = Config::get('app.js_url');
$jsLibUrl = Config::get('app.js_lib_url');
$jsPluginUrl = Config::get('app.js_plugin_url');
$jsDirectiveUrl = Config::get('app.js_directive_url');
$jsFactoryUrl = Config::get('app.js_factory_url');
$jsControllerUrl = Config::get('app.js_controller_url');
$stripePublishableKey = Config::get('services.stripe.public');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js" ng-app="videoInsightViewer">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>TechTank.eu | Error</title>
    <meta content="A simple way to share your videos and keep track of who exactly interacts with them" name="description" />

    <link rel="apple-touch-icon" sizes="57x57" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    @include('techtank.includes.css')

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="viewer">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a>
        <img src="https://s3-eu-west-1.amazonaws.com/videoinsight.io/cdn/images/logos/videoinsightio-logo-red.png" alt="logo" class="logo-default"/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

    @yield('content')

</div>

<div class="copyright">
    2015 © Techtank.eu
</div>

@include('techtank.includes.external-js')

</div>

</body>
<!-- END BODY -->
</html>