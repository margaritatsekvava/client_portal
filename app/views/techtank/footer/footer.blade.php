<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        Copyright © <?php echo date("Y"); ?> <a href="https://www.techtank.eu" target="_blank">TECHTANK.eu</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->