<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">

<div class="page-sidebar navbar-collapse collapse">
<!-- BEGIN SIDEBAR MENU -->

<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" data-ng-controller="NavController">

    <li class="start" data-ng-class="navClass('home')">
        <a data-ng-href='#/home' >
            <i class="icon-home"></i>
            <span class="title">Home</span>

        </a>
    </li>

    <li data-ng-class="navClass('video')">
        <a data-ng-href='#/video' >
            <i class="icon-social-youtube"></i>
            <span class="title">Videos</span>
        </a>
    </li>

    <li data-ng-class="navClass('visitor')">
        <a data-ng-href='#/visitor' >
            <i class="icon-users"></i>
            <span class="title">Visitors</span>
        </a>
    </li>

    <li data-ng-class="navClass('help')">
        <a href="https://videoinsight.helpdocs.com/" >
            <i class="icon-question"></i>
            <span class="title">Help</span>
        </a>
    </li>

    <li data-ng-class="navClass('account')">
        <a data-ng-href='#/account' >
            <i class="icon-settings"></i>
            <span class="title">Account</span>
        </a>
    </li>

    <li>
        {{ HTML::decode(HTML::linkRoute('logout_path', '<i class="icon-logout"></i><span class="title">Log out</span>', array(), array())) }}
    </li>

</ul>
<!-- END SIDEBAR MENU -->
</div>
</div>
<!-- END SIDEBAR -->