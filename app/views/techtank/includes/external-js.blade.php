<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>

{{ HTML::script('js/respond.min.js') }}
{{ HTML::script('js/excanvas.min.js') }}

<![endif]-->

<!-- Libs -->
{{ HTML::script('js/libs/jquery/jquery.min.js') }}
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

{{ HTML::script('js/libs/jquery/jquery-ui.min.js') }}

<!-- Plugins -->

{{ HTML::script('js/plugins/bootstrap/bootstrap.js') }}
{{ HTML::script('js/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js') }}
{{ HTML::script('js/plugins/bootstrap-switch/bootstrap-switch.js') }}
{{ HTML::script('js/plugins/pnotify/jquery.pnotify.js') }}
{{ HTML::script('js/plugins/jquery-slimscroll/jquery.slimscroll.js') }}
{{ HTML::script('js/plugins/jquery.blockui.min.js') }}
{{ HTML::script('js/plugins/jquery.cokie.min.js') }}
{{ HTML::script('js/plugins/uniform/jquery.uniform.js') }}
{{ HTML::script('js/plugins/jquery-validation/jquery.validate.js') }}
{{ HTML::script('js/plugins/jquery-validation/additional-methods.js') }}
{{ HTML::script('js/plugins/emoticons/jquery.cssemoticons.js') }}



<!-- App -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{ HTML::script('js/metronic.js') }}
{{ HTML::script('js/layout.js') }}
<!-- END PAGE LEVEL SCRIPTS -->


<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init Metronic core componets
        Layout.init(); // init layout
    });
</script>




