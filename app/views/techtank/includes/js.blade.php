<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<!--<script src="{{$jsPluginUrl}}/respond.min.js"></script>-->
<!--<script src="{{$jsPluginUrl}}/excanvas.min.js"></script>-->
<![endif]-->

<!-- Libs -->
<script src="{{$jsUrl}}/libs/angular/angular.js"></script>
<script src="{{$jsUrl}}/libs/underscore/underscore-1.4.1.js"></script>
<script src="{{$jsUrl}}/libs/angular/angular-route.js"></script>
<script src="{{$jsUrl}}/libs/angular/angular-sanitize.js"></script>
<script src="{{$jsUrl}}/libs/jquery/jquery.min.js" type="text/javascript"></script>
<script src="{{$jsUrl}}/libs/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{$jsUrl}}/libs/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


<script>
    Stripe.setPublishableKey('<?php echo $stripePublishableKey; ?>');
</script>


<!-- Plugins -->
<script src="{{$jsPluginUrl}}/bootstrap/bootstrap.js" type="text/javascript"></script>
<script src="{{$jsPluginUrl}}/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js" type="text/javascript"></script>
<script src="{{$jsPluginUrl}}/bootstrap-switch/bootstrap-switch.js" type="text/javascript"></script>
<script src="{{$jsPluginUrl}}/pnotify/jquery.pnotify.js"></script>
<script src="{{$jsPluginUrl}}/pusher/pusher.js"></script>
<script src="{{$jsPluginUrl}}/xeditable/xeditable.js"></script>

<script src="{{$jsPluginUrl}}/videogular/videogular.js"></script>
<script src="{{$jsPluginUrl}}/videogular/vg-controls.js"></script>
<script src="{{$jsPluginUrl}}/videogular/vg-overlay-play.js"></script>
<script src="{{$jsPluginUrl}}/videogular/vg-poster.js"></script>
<script src="{{$jsPluginUrl}}/videogular/vg-buffering.js"></script>
<script src="{{$jsPluginUrl}}/videogular/vg-analytics.js"></script>

<script src="{{$jsPluginUrl}}/angular-validation/angular-validation.js"></script>
<script src="{{$jsPluginUrl}}/angular-validation/angular-validation-rule.js"></script>

<script src="{{$jsPluginUrl}}/angular-payments/angular-payments.js"></script>



<script src="{{$jsPluginUrl}}/angular-loading/angular-loading-bar.js"></script>
<script src="{{$jsPluginUrl}}/angular-strap/angular-strap.js"></script>
<script src="{{$jsPluginUrl}}/angular-strap/angular-strap.tpl.js"></script>

<script src="{{$jsPluginUrl}}/jquery-slimscroll/jquery.slimscroll.js" type="text/javascript"></script>
<script src="{{$jsPluginUrl}}/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{$jsPluginUrl}}/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{$jsPluginUrl}}/uniform/jquery.uniform.js" type="text/javascript"></script>
<script src="{{$jsPluginUrl}}/emoticons/jquery.cssemoticons.js" type="text/javascript" ></script>
<script src="{{$jsPluginUrl}}/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript" ></script>
<script src="{{$jsPluginUrl}}/bootstrap-switch/bootstrap-switch.js" type="text/javascript" ></script>

<!-- END CORE PLUGINS -->

<script src="{{$jsPluginUrl}}/angular-upload/angular-file-upload.js"></script>
<script src="{{$jsPluginUrl}}/bootstrap-toastr/toastr.js"></script>

<!-- Directives -->
<script src="{{$jsUrl}}/directives/angular-slimscroll.js" type="text/javascript"></script>
<script src="{{$jsUrl}}/directives/ng-google-chart.js" type="text/javascript"></script>
<script src="{{$jsUrl}}/directives/ngThumb.js" type="text/javascript"></script>
<script src="{{$jsUrl}}/directives/ng-password.js" type="text/javascript"></script>

<!-- App -->
<script src="{{$jsUrl}}/metronic.js" type="text/javascript"></script>
<script src="{{$jsUrl}}/layout.js" type="text/javascript"></script>
<script src="{{$jsUrl}}/main.js"></script>

<script type="text/javascript">
    techtankApp.constant('front_end_url', '<?php echo $frontEndUrl;?>');
    techtanktApp.constant('environment', '<?php echo $environment;?>');
    techtanktApp.constant('api_url', '<?php echo $apiUrl;?>');
    techtanktApp.constant('upload_end_point_url', '<?php echo $uploadEndPointUrl;?>');
    techtanktApp.constant('csrf_token', '<?php echo $csrfToken;?>');
    techtanktApp.constant('version','<?php echo $version;?>');
    techtanktApp.constant('local_path','<?php echo $localPath;?>');

</script>
<!-- END JAVASCRIPTS -->

<!-- Providers -->
<script src="{{$jsUrl}}/providers/pusher/angular-pusher.js"></script>

<!-- Directives -->

<!-- Controllers -->
<script src="{{$jsUrl}}/controllers/notification/notification.controller.js"></script>
<script src="{{$jsUrl}}/controllers/nav/nav.controller.js"></script>
<script src="{{$jsUrl}}/controllers/home/home.controller.js"></script>
<script src="{{$jsUrl}}/controllers/upload/upload.controller.js"></script>

<script src="{{$jsUrl}}/controllers/account/account.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountAvatar.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountPassword.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBilling.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBillingNewSubscription.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBillingCreateSubscription.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBillingUpdateSubscription.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBillingCancelSubscription.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBillingRenewSubscription.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBillingUpdatePayment.controller.js"></script>
<script src="{{$jsUrl}}/controllers/account/accountBillingViewInvoice.controller.js"></script>


<script src="{{$jsUrl}}/controllers/video/videoLibrary.controller.js"></script>
<script src="{{$jsUrl}}/controllers/video/video.controller.js"></script>

<script src="{{$jsUrl}}/controllers/visitor/visitor.controller.js"></script>
<script src="{{$jsUrl}}/controllers/visitor/visitorInfo.controller.js"></script>

<script src="{{$jsUrl}}/controllers/videoInteraction/videoInteraction.controller.js"></script>

<!-- Factories -->
<script src="{{$jsUrl}}/factories/notification/notification.factory.js"></script>
<script src="{{$jsUrl}}/factories/user/user.factory.js"></script>
<script src="{{$jsUrl}}/factories/video/video.factory.js"></script>
<script src="{{$jsUrl}}/factories/visitor/visitor.factory.js"></script>
<script src="{{$jsUrl}}/factories/videoInteraction/videoInteraction.factory.js"></script>

<!-- General -->
<script src="{{$jsUrl}}/factories/general/responseValidator.factory.js"></script>
<script src="{{$jsUrl}}/factories/general/calculation.factory.js"></script>


<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
    });
</script>

<!--<script>-->
<!--    //<![CDATA[-->
<!--    (function() {var s=document.createElement('script');-->
<!--        s.type='text/javascript';s.async=true;-->
<!--        s.src=('https:'==document.location.protocol?'https':'http') +-->
<!--            '://videoinsight.groovehq.com/widgets/ddd17848-1ec6-4992-90ab-574439e717f5/ticket.js'; var q = document.getElementsByTagName('script')[0];q.parentNode.insertBefore(s, q);})();-->
<!--    //]]>-->
<!--</script>-->








