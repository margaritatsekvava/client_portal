<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

{{ HTML::style('css/font-awesome/font-awesome.css') }}
{{ HTML::style('css/simple-line-icons/simple-line-icons.css') }}
{{ HTML::style('css/bootstrap/bootstrap.css') }}
{{ HTML::style('css/uniform/uniform.default.css') }}
{{ HTML::style('css/bootstrap-switch/bootstrap-switch.css') }}
{{ HTML::style('css/bootstrap-fileinput/bootstrap-fileinput.css') }}
{{ HTML::style('css/emoticons/jquery.cssemoticons.css') }}
{{ HTML::style('css/bootstrap-toastr/toastr.css') }}
{{ HTML::style('css/videogular/videogular.css') }}
{{ HTML::style('css/xeditable/xeditable.css') }}


<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
{{ HTML::style('css/components-rounded.css') }}
{{ HTML::style('css/plugins.css') }}
{{ HTML::style('css/layout.css') }}
{{ HTML::style('css/style.css') }}
<!-- END THEME STYLES -->

<!-- Blueimp styles -->

{{ HTML::style('css/blueimp/jquery.fileupload.css') }}
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript>
    {{ HTML::style('css/blueimp/jquery.fileupload-noscript.css') }}
</noscript>
<noscript>
    {{ HTML::style('css/blueimp/jquery.fileupload-ui-noscript.css') }}
</noscript>