@extends('techtank.layouts.externalmaster')

@section('content')

<!-- BEGIN LOGIN FORM -->
{{ Form::open(array('route' => 'login_path', 'class'=>'login-form', 'id'=>'form-login')) }}

<input type="hidden" id="redirect" name="redirect"/>

    <h3 class="form-title">Sign In</h3>
    @include('flash::message')
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        {{ Form::email('email',null,['id'=>'login-email','class'=>'form-control form-control-solid placeholder-no-fix','placeholder'=>'Email','required'=>'required']) }}

    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        {{ Form::password('password',['id'=>'login-password','class'=>'form-control form-control-solid placeholder-no-fix','placeholder'=>'Password','required'=>'required']) }}
    </div>
    <div class="form-actions">
        {{ Form::submit('Login', array('id'=>'btnlogin','class'=>'btn btn-success uppercase')) }}

        <label class="rememberme check">
         <input type="checkbox" name="remember" value="1"/>Remember </label>
        {{ HTML::decode(HTML::linkRoute('password_reminder', '<strong>Forgot Password?</strong>', array(), array('class'=>'forget-password'))) }}
    </div>



<script>
    document.getElementById("redirect").value = window.location.hash;
</script>


{{ Form::close() }}
<!-- END LOGIN FORM -->
@stop


