@extends('techtank.layouts.externalmaster')

@section('content')


<div class="register-container full-height sm-p-t-30">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <img src="{{{ asset('assets/img/uniwink-logo-2x.png') }}}" alt="logo" data-src="{{{ asset('assets/img/uniwink-logo-2x.png') }}}" data-src-retina="{{{ asset('assets/img/uniwink-logo-2x.png') }}}"
                <br/>
                <h3>Facebook Profile sync</h3>

                <p>
                    Enter your e-mail address below to sync the following social profile with your uniwink profile.
                </p>

                {{ Form::open(array('route' => 'social_sync_facebook_path', 'class'=>'', 'id'=>'form-social-sync')) }}

                @include('flash::message')

                <input type="hidden" id="identifier" name="identifier" value="{{$social_profile->identifier}}"/>

                <div class="row">
                    <div class="col-sm-12">

                        <span class="thumbnail-wrapper d48 circular inline m-t-5">
                            <img src="{{ $social_profile->photo_url }}" alt="profile photo" data-src="{{ $social_profile->photo_url }}" data-src-retina="{{ $social_profile->photo_url }}" width="32" height="32"/>
                        </span>

                        <div class="p-r-10 p-l-60 p-t-20 fs-16 font-heading">
                            <span class="semi-bold">{{ $social_profile->display_name }}</span>

                            - Is this you?
                        </div>

                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-default">
                            <label>Email address(Used in uniwink)</label>
                            {{ Form::email('email',null,['id'=>'email','class'=>'form-control placeholder-no-fix','placeholder'=>'Email','required'=>'required']) }}
                        </div>
                    </div>
                </div>

                <div class="form-group margin-top-20 margin-bottom-20 text-right">

                    {{ HTML::decode(HTML::linkRoute('login', '<strong>Back</strong>', array(), array('class'=>'btn btn-default'))) }}
                    &nbsp;&nbsp;&nbsp;
                    {{ Form::submit('Sync profile', array('id'=>'sync-profile','class'=>'btn btn-success uppercase pull-right')) }}

                </div>


           {{ Form::close() }}

        </div>
    </div>
</div>


@stop



