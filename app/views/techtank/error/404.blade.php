@extends('videoinsightio.layouts.error')


@section('content')


<div class="col-md-12 page-404">
    <div class=" number">
       404
    </div>
    <div class=" details">
        <h3>Oops! You're lost. The page you were looking for was not found</h3>
    </div>
</div>



@stop