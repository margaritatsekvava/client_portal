@extends('videoinsightio.layouts.error')


@section('content')


<div class="col-md-12 page-500">
    <div class="number">
        Browser issue
    </div>
    <div class=" details">
        <h3>Oops! Unfortunately we do not support your browser.</h3>

        <br/>

        <h3>Please try either Google Chrome, Internet Explorer, Firefox or Safari.</h3>

        <p class="text-center">
            <a href="http://www.google.com/chrome/">Download Chrome</a>
            <br/>
            <a href="http://windows.microsoft.com/en-IE/internet-explorer/download-ie">Download Internet Explorer</a>
            <br/>
            <a href="https://www.mozilla.org/en-US/firefox/new/">Download Firefox</a>
            <br/>
            <a href="https://www.apple.com/safari/">Download Safari</a>
        </p>

    </div>
</div>


@stop