@extends('videoinsightio.layouts.error')


@section('content')


<div class="col-md-12 page-500">
    <div class=" number">
        500
    </div>
    <div class=" details">
        <h3>Oops! Something went wrong.</h3>

               <pre class="line-numbers">
                <code class="language-markup">
                    {{ $exception }}

                </code>
            </pre>
    </div>
</div>


@stop