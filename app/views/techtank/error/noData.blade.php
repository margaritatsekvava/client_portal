<div class="row">
    <div class="col-xs-12 text-center">
        <h1><i class="fa fa-exclamation-triangle text-info animation-pulse"></i> No data</h1>
        <h2 class="h3">Oops, we are sorry but your request contains no data</h2>
    </div>
    <br/>
</div>