@extends('emails.layouts.default')
@section('content')
<!--========================================================================================================-->
<!-- Start of banner -->
<!--========================================================================================================-->
<table width="100%" bgcolor="#272b35" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" class="">
    <tbody>
    <tr>
        <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Top Spacing -->
                <tr>
                    <td width="100%" height="40" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                </tr>
                <!-- Top Spacing -->
                <tr>
                    <td>
                        <table width="600" cellpadding="0" cellspacing="0" border="0" valign="middle" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="devicewidthinner">
                            <tbody>
                            <!-- Title -->
                            <tr>
                                <td style="font-family: 'Ubuntu', sans-serif; font-weight: 300; color: rgb(255, 255, 255); text-align: center; line-height: 34px; font-size: 39px;" class="">Techtank</td>
                            </tr>
                            <!-- /Title -->
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="30" style="font-size: 0;line-height: 0;border-collapse: collapse;" class="">&nbsp;</td>
                            </tr>
                            <!-- /Spacing -->
                            <!-- Para -->
                            <tr>
                                <td style="font-family: 'Ubuntu', sans-serif; font-weight: 300; color: rgb(255, 255, 255); text-align: center; line-height: 20px; font-size: 20px;" class="">IT and Business Strategy Services</td>
                            </tr>
                            <!-- /Para -->
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="40" style="font-size: 0;line-height: 0;border-collapse: collapse;" class="">&nbsp;</td>
                            </tr>
                            <!-- /Spacing -->

                            </tbody>
                        </table>
                    </td>
                </tr>
                <!-- Bottom Spacing -->
                <tr>
                    <td width="100%" height="0" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                </tr>
                <!-- Bottom Spacing -->
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!--========================================================================================================-->
<!-- End of banner -->
<!--========================================================================================================-->

<!--========================================================================================================-->
<!-- Start of fullwidth-text -->
<!--========================================================================================================-->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" class="">
    <tbody>
    <tr>
        <td width="100%">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" bgcolor="#ffffff">
                <tbody>
                <!-- Top Spacing -->
                <tr>
                    <td width="100%" height="40" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                </tr>
                <!-- Top Spacing -->
                <tr>
                    <td width="100%">
                        <table width="580" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="devicewidthinner">
                            <tbody>
                            <!-- Title -->
                            <tr>
                                <td style="font-family: 'Ubuntu', sans-serif; font-size: 30px; font-weight: 300; color: #333333; text-align: center; line-height: 37px;" class="">Hi, <b>{{ $first_name }} {{$last_name}}</b></td>
                            </tr>
                            <!-- /Title -->
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;" class="">&nbsp;</td>
                            </tr>
                            <!-- /Spacing -->
                            <!-- Para -->
                            <tr>
                                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px; font-weight: 300; color: rgb(102, 102, 102); line-height: 24px; text-align: left;" class="" align="left">

                                    <br><b>New progress is created for project "{{$project_name}}".<b><br>

                                            Progress is: {{$percentage}} %.
                                            Comment: {{$comment}}



                                            <br></b>Best regards,<br><b>The Techtank Team<br><br></b></td>
                            </tr>

                            <!-- /Para -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                <!-- Bottom Spacing -->
                <tr>
                    <td width="100%" height="40" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                </tr>
                <!-- Bottom Spacing -->
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!--========================================================================================================-->
<!-- End of fullwidth-text -->
<!--========================================================================================================-->

@stop
























