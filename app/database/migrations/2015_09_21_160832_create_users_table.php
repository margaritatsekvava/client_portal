<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function ($table) {
            $table->increments('user_id');
            $table->string('email', 200)->unique();
            $table->string('password');
            $table->string('first_name', 200)->default('');
            $table->string('last_name', 200)->default('');
            $table->string('status', 50)->default('Verified');
            $table->string('avatar', 300)->default('');
            $table->string('company')->default('');
            $table->string('address', 300)->default('');
            $table->string('city', 200)->default('');
            $table->string('country')->default('');
            $table->string('account_type')->default('Client');
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('users');
	}

}
