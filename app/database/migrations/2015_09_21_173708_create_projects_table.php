<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('projects', function ($table) {
            $table->increments('project_id');
            $table->string('name', 200)->default('');
            $table->string('description', 400)->default();
            $table->string('start_date');
            $table->string('end_date');
            $table->string('status', 50)->default('');
            $table->integer('client_id')->unsigned();
            $table->integer('admin_id')->unsigned();

            $table->timestamps();

            $table->foreign('client_id')->references('user_id')->on('users');
            $table->foreign('admin_id')->references('user_id')->on('users');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		schema:drop('projects');
	}

}
