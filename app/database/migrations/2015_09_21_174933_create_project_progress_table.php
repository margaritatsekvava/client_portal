<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectProgressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('project_progress', function ($table) {
            $table->increments('project_progress_id');
            $table->integer('project_id')->unsigned();
            $table->integer('percentage');
            $table->string('comment', 400)->default('');

            $table->timestamps();

            $table->foreign('project_id')->references('project_id')->on('projects');


        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		schema::drop('project_progress');
	}

}
