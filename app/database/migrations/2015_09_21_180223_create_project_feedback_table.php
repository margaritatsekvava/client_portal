<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFeedbackTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('project_feedback', function ($table) {
            $table->increments('project_feedback_id');
            $table->integer('rating')->unsigned();
            $table->string('quotation',400);
            $table->integer('project_id')->unsigned();

            $table->timestamps();

            $table->foreign('project_id')->references('project_id')->on('projects');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		schema::drop('project_feedback');
	}

}
