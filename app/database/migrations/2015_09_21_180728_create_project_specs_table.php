<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSpecsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('project_specs', function ($table) {
            $table->increments('project_spec_id');
            $table->string('name', 200)->default('');
            $table->string('location', 500)->default('');
            $table->string('status', 50)->default('');
            $table->string('thumbnail', 300)->default('');
            $table->integer('project_id')->unsigned();

            $table->timestamps();

            $table->foreign('project_id')->references('project_id')->on('projects');


        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		schema::drop('project_specs');
	}

}
