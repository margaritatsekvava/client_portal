<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('twitter_profiles', function ($table) {
            $table->increments('twitter_profile_id');
            $table->string('identifier',100)->default('');
            $table->string('website_url', 300)->default('')->nullable();
            $table->string('profile_url', 300)->default('')->nullable();
            $table->string('photo_url', 300)->default('')->nullable();
            $table->string('display_name', 100)->default('')->nullable();
            $table->string('description', 900)->default('')->nullable();
            $table->string('first_name', 100)->default('')->nullable();
            $table->string('last_name', 100)->default('')->nullable();
            $table->string('gender', 50)->default('')->nullable();
            $table->string('language', 50)->default('')->nullable();
            $table->string('age', 50)->default('')->nullable();
            $table->string('birth_day', 50)->default('')->nullable();
            $table->string('birth_month', 50)->default('')->nullable();
            $table->string('birth_year', 50)->default('')->nullable();
            $table->string('email', 200)->default('')->nullable();
            $table->string('email_verified', 200)->default('')->nullable();
            $table->string('phone', 50)->default('')->nullable();
            $table->string('address', 100)->default('')->nullable();
            $table->string('country', 50)->default('')->nullable();
            $table->string('region', 50)->default('')->nullable();
            $table->string('city', 50)->default('')->nullable();
            $table->string('zip', 20)->default('')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('user_id')->on('users');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		schema::drop('twitter_profiles');
	}

}
