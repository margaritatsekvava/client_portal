<?php


class ProjectFeedbackTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('project_feedback')->insert(
            array(

                array(
                    'rating'=>5,
                    'quotation'=>'',
                    'project_id'=>1,
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),

                array(
                    'rating'=>4,
                    'quotation'=>'',
                    'project_id'=>2,
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),





            ));

    }

}

