<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

        DB::table('users')->insert(
            array(

                array(
                    'email'=>'glenn@ottera.com',
                    'password'=>Hash::make('glenn'),
                    'first_name'=>'Glenn',
                    'last_name'=>'Goggin',
                    'status'=>'Verified',
                    'avatar'=>'https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAALOAAAAJDdjMzUzODdlLWZlZGUtNDI3OC04Y2Y3LTFjMjc0MTdhNzEzYg.jpg',
                    'company'=>'Ottera',
                    'address'=>'74 South Mall',
                    'city'=>'Cork',
                    'country'=>'Ireland',
                    'account_type'=>'admin',
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),

                array(
                    'email'=>'uniwink@ottera.com',
                    'password'=>Hash::make('uniwink'),
                    'first_name'=>'Uniwink',
                    'last_name'=>'User',
                    'status'=>'Verified',
                    'avatar'=>'https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAALOAAAAJDdjMzUzODdlLWZlZGUtNDI3OC04Y2Y3LTFjMjc0MTdhNzEzYg.jpg',
                    'company'=>'Ottera',
                    'address'=>'74 South Mall',
                    'city'=>'Cork',
                    'country'=>'Ireland',
                    'account_type'=>'client',
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),

                array(
                    'email'=>'margarita@ottera.com',
                    'password'=>Hash::make('margarita'),
                    'first_name'=>'Margarita',
                    'last_name'=>'Tsekvava',
                    'status'=>'Verified',
                    'avatar'=>'https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAALOAAAAJDdjMzUzODdlLWZlZGUtNDI3OC04Y2Y3LTFjMjc0MTdhNzEzYg.jpg',
                    'company'=>'Ottera',
                    'address'=>'74 South Mall',
                    'city'=>'Cork',
                    'country'=>'Ireland',
                    'account_type'=>'client',
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),





            ));





	}

}
