<?php


class ProjectTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('projects')->insert(
            array(

                array(
                    'name'=>'uniwink',
                    'description'=>'Notes sharing web application',
                    'start_date'=>'2015-12-10 00:00:00',
                    'end_date'=>'2016-12-02 00:00:00',
                    'status'=>'Enabled',
                    'client_id'=>'2',
                    'admin_id'=>'1',
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),

                array(
                    'name'=>'video insight',
                    'description'=>'Video analytic Software ',
                    'start_date'=>'2015-10-09 00:00:00',
                    'end_date'=>'2016-11-05 00:00:00',
                    'status'=>'Enabled',
                    'client_id'=>'2',
                    'admin_id'=>'1',
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),





            ));





    }

}

