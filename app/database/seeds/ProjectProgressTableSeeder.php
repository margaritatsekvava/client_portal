<?php


class ProjectProgressTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

          DB::table('project_progress')->insert(
            array(

                array(
                    'project_id'=>1,
                    'percentage'=>20,
                    'comment'=>'20 percent completed',
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),

                array(
                    'project_id'=>2,
                    'percentage'=>55,
                    'comment'=>'55% is completed',
                    'created_at'=>date('Y-m-d H:m:s'),
                    'updated_at'=>date('Y-m-d H:m:s')
                ),





            ));





    }

}

