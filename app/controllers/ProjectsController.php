<?php
class ProjectsController extends ApiController{

    private $messages = [

        'readProjectInfo.false.no_project'=>'Hey, this project does not exist :/',
        'readProjectInfo.false.no_project_id'=>'Hey, this is strange. You need to give us the project_id so we can search for it. :)',

        'readClientProjects.false.no_user'=>'Hey, this user does not exit:/',
        'readClientProjects.false.no_client'=>'Hey, This user is an admin not a client. You need to give us the client_id so we can search for it. :)',
        'readClientProjects.false.no_user_id'=>'Hey, this is strange. You need to give us the client_id so we can search for it. :)',
        'readClientProjects.false.no_project'=>'Hey, this client has no project:/',

        'readAdminProjects.false.no_user'=>'Hey, this user does not exit:/',
        'readAdminProjects.false.no_client'=>'Hey, This user is a client not an admin. You need to give us the admin_id so we can search for it. :)',
        'readAdminProjects.false.no_user_id'=>'Hey, this is strange. You need to give us the admin_id so we can search for it. :)',
        'readAdminProjects.false.no_project'=>'Hey, this admin has no project:/',

        'createProject.true' => 'Hey congrats, Project is successfully created.)',
        'createProject.false' => 'Ok, this is embarrassing, something weird happened when we tried to create a Project for you. Dont worry, we are on it ! :)',
        'createProject.admin_permission' => 'Sorry, You have not permission to create a Project. Only Admin can create a Project ',
        'createProject.no_user_id' => 'Hey, this is strange. You need to give us the admin_id so we can create a project for it. :)',

        'updateProject.true' => 'Great stuff, you have successfully updated your Project. :)',
        'updateProject.false.db_error' => 'Ok, this is embarrassing, something weird happened when we tried to update your account. Dont worry we are on it. #awkard :/',
        'updateProject.false.no_project' => 'Hey, this project doesnt exist :/',
        'updateProject.false.no_project_id' => 'Hey, this is strange. You need to give us the project_id so we can update it. :)',


    ];

    public function createProject(){


            $project = new Project;
            $project->name = Input::get('name');
            $project->description = Input::get('description');
            $project->start_date = Input::get('start_date');
            $project->end_date = Input::get('end_date');
            $project->status = Input::get('status');
            $project->client_id = Input::get('client_id');
            $project->admin_id = Input::get('admin_id');
            $project->created_at = Carbon::now();
            $project->updated_at = Carbon::now();

            if( $project->save()){
                $client=User::find($project->client_id);
                Event::fire('projectCreated', array($client,$project));
                return $this->recordCreatedResponse($this->messages["createProject.true"],$project->project_id,'true');
            }
            else{
                return $this->otherValidationFailureResponse($this->messages["createProject.false"]);
            }
    }



    public function updateProject($project_id)
    {
        if ($project_id) {
            $project = Project::Find($project_id);
            if ($project) {

                $project->name = Input::get('name');
                $project->description = Input::get('description');
                $project->start_date = Input::get('start_date');
                $project->end_date = Input::get('end_date');
                $project->status = Input::get('status');
                $project->client_id = Input::get('client_id');
                $project->admin_id = Input::get('admin_id');
                $project->updated_at = Carbon::now();
                $response = $project->forceSave();

                if ($response === true) {
                    //SegmentEventHandler::handleAccountUpdatedEvent($user);
                    return $this->recordUpdatedResponse($this->messages["updateProject.true"], $project_id, 'true');
                } else {
                    return $this->recordUpdatedResponse($this->messages["updateProject.false.db_error"], $project_id, 'false');
                }

            } else {
                return $this->otherValidationFailureResponse($this->messages["updateProject.false.no_project"]);
            }

        } else {
            return $this->otherValidationFailureResponse($this->messages["updateProject.false.no_project_id"]);
        }
    }





    public function readProjectInfo($project_id){

        if($project_id){
            $project = Project::find($project_id);

            if($project){

                $project_collection = $project;
                $project->progress = $project_collection->progress;
                $project->latest_progress = $project_collection->latestProgress;
                $project->feedback = $project_collection->feedback;
                $project->spec = $project_collection->spec;
                return $this->respondWithData('projects',$project);
            }
            else{
                return $this->otherValidationFailureResponse($this->messages["readProjectInfo.false.no_project"]);
            }

        }

        else{
            return $this->otherValidationFailureResponse($this->messages["readProjectInfo.false.no_project_id"]);
        }
    }



    public function readClientProjects($client_id){

        if($client_id){

            $client = User::find($client_id);

            if($client){

                $projects = Project::where('client_id', '=', $client_id)->orderBy('created_at','asc')->get();

                    if($projects){

                        foreach($projects as $project){

                            $project_collection = Project::find($project->project_id);

                            $project->progress = $project_collection->progress;
                            $project->latest_progress = $project_collection->latestProgress;
                            $project->feedback = $project_collection->feedback;
                            $project->spec = $project_collection->spec;


                        }

                        return $this->respondWithData('projects',$projects);

                    }
                    else{
                        return $this->otherValidationFailureResponse($this->messages["readClientProjects.false.no_project"]);

                    }

            }
            else{
                return $this->otherValidationFailureResponse($this->messages["readClientProjects.false.no_user"]);
            }
        }
        else{
            return $this->otherValidationFailureResponse($this->messages["readClientProjects.false.no_user_id"]);

        }
    }


    public function readAdminProjects($admin_id){

        if($admin_id){

            $admin = User::find($admin_id);

            if($admin){

                    $projects = Project::where('admin_id', '=', $admin_id)->orderBy('created_at','asc')->get();

                    if($projects){

                        foreach($projects as $project){

                            $project_collection = Project::find($project->project_id);

                            $project->progress = $project_collection->progress;
                            $project->latest_progress = $project_collection->latestProgress;
                            $project->feedback = $project_collection->feedback;
                            $project->spec = $project_collection->spec;


                        }

                        return $this->respondWithData('projects',$projects);

                    }
                    else{
                        return $this->otherValidationFailureResponse($this->messages["readAdminProjects.false.no_project"]);

                    }


            }
            else{
                return $this->otherValidationFailureResponse($this->messages["readAdminProjects.false.no_user"]);
            }
        }
        else{
            return $this->otherValidationFailureResponse($this->messages["readAdminProjects.false.no_user_id"]);

        }
    }

    public function readClientLatestProject($client_id){

        if($client_id){

            $client = User::find($client_id);

            if($client){

                    $projects = Project::where('client_id', '=', $client_id)->orderBy('created_at','asc')->get();

                    if(!$projects->isEmpty()){
                        $latestProject = $projects->first();

                        $project_collection = Project::find($latestProject->project_id);

                        $latestProject->progress = $project_collection->progress;
                        $latestProject->latest_progress = $project_collection->latestProgress;
                        $latestProject->feedback = $project_collection->feedback;
                        $latestProject->spec = $project_collection->spec;



                        return $this->respondWithData('projects',$latestProject);
                    }
                    else{
                        return $this->otherValidationFailureResponse($this->messages["readClientProjects.false.no_project"]);

                    }

            }
            else{
                return $this->otherValidationFailureResponse($this->messages["readClientProjects.false.no_user"]);
            }
        }
        else{
            return $this->otherValidationFailureResponse($this->messages["readClientProjects.false.no_user_id"]);

        }
    }

    public function readAdminLatestProject($admin_id){

        if($admin_id){

            $admin = User::find($admin_id);

            if($admin){

                    $projects = Project::where('admin_id', '=', $admin_id)->orderBy('created_at','asc')->get();

                    if(!$projects->isEmpty()){
                        $latestProject = $projects->first();

                        $project_collection = Project::find($latestProject->project_id);

                        $latestProject->progress = $project_collection->progress;
                        $latestProject->latest_progress = $project_collection->latestProgress;
                        $latestProject->feedback = $project_collection->feedback;
                        $latestProject->spec = $project_collection->spec;

                        return $this->respondWithData('projects',$latestProject);
                    }
                    else{
                        return $this->otherValidationFailureResponse($this->messages["readAdminProjects.false.no_project"]);

                    }

            }
            else{
                return $this->otherValidationFailureResponse($this->messages["readAdminProjects.false.no_user"]);
            }
        }
        else{
            return $this->otherValidationFailureResponse($this->messages["readAdminProjects.false.no_user_id"]);

        }
    }

}