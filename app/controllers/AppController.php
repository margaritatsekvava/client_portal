<?php

/**
 * Class AppController
 */
class AppController extends \BaseController {

    function __construct(){

        $this->beforeFilter('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexView()
    {
       return View::make('techtank.layouts.master');
    }

}