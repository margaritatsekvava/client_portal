<?php
class ProjectSpecsController extends ApiController{

    private $messages = [

       'specUpload.false.no_project' => 'Hey, this project doesnt exist :/',
        'specUpload.false.no_project_id' => 'Hey, this is strange. You need to give us the project_id so we can update it. :)',
        'specUpload.true' => 'Hey congrats, Project Specs is successfully created.)',
        'specUpload.false' => 'Ok, this is embarrassing, something weird happened when we tried to Upload a Project Spec for you. Dont worry, we are on it ! :)',
        'specUpload.false.no_file' => 'Ok, No file chosen. You need to choose the file to upload the project spec',

    ];


    public function specsUpload($project_id)
    {
        if ($project_id) {
            $project = Project::find($project_id);
            if ($project) {

                $spec = new ProjectSpec;
                $spec->project_id = $project_id;
                $spec->name = Input::get('name');
                $spec->location = 'C:\wamp\www\client_portal\app\storage\specs\Course_Outline.pdf';

                $spec->status = 'Not Uploaded';
                $spec->thumbnail = '';
                $spec->created_at = Carbon::now();
                $spec->updated_at = Carbon::now();

               if ($spec->save())
                    $spec_id = $spec->project_spec_id;
                if (!empty($_FILES)) {

                    $fileName = $_FILES['location']['name'];

                    $tempPath = $_FILES['location']['tmp_name'];


                    $uploadDirectory = 'C:\wamp\www\client_portal\app\storage\specs' . DIRECTORY_SEPARATOR . $project_id;

                    $old = umask(0);

                    if (file_exists($uploadDirectory) == false) {
                        if (is_dir($uploadDirectory)) {
                            mkdir($uploadDirectory, 0775, true);
                        } else {
                            mkdir($uploadDirectory, 0775, true);
                        }
                    }

                    $uploadFilePath = $uploadDirectory . DIRECTORY_SEPARATOR . $fileName;

                    $moveFile = move_uploaded_file($tempPath, $uploadFilePath);

                    if ($moveFile) {


                        umask($old);
                        $specFile = $uploadFilePath;


                        // update user record

                        $oldfile =$spec->location;
                        $spec->location = $specFile;
                        $spec->status='Uploaded';
                        $spec->updated_at = Carbon::now();
                        $response = $spec->forceSave();


                        if ($response === true) {
                           $admin_id =$project->admin_id;
                            $admin = User::find($admin_id);
                            Event::fire('projectSpecUploaded',array($admin,$project));
                            return $this->recordUpdatedResponse($this->messages["specUpload.true"], $spec_id, 'true');
                            //return $admin;
                            //return $this->otherValidationFailureResponse($this->messages["createUser.false.photo_db_error"]);
                        }

                        //$this->removeLocalFiles($uploadDirectory);

                    } else {
                        return $this->otherValidationFailureResponse($this->messages["specUpload.false"]);
                    }

                }else {
                    return $this->otherValidationFailureResponse($this->messages["specUpload.false.no_file"]);

                }

            }

            else {
                return $this->otherValidationFailureResponse($this->messages["specUpload.false.no_project"]);
            }
        }else {
            return $this->otherValidationFailureResponse($this->messages["specUpload.false.no_project_id"]);
        }

    }


}