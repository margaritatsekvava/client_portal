<?php

use techtank\handlers\UserEventHandler;
class UsersController extends ApiController{

    private $messages = [
        'createUser.true' => 'Hey congrats, Account is successfully created.)',
        'createUser.false' => 'Ok, this is embarrassing, something weird happened when we tried to create an account for you. Dont worry, we are on it ! :)',

        'create.false.photo_db_error' => 'Ok, this is embarrassing, something weird happened when we tried to update your profile photo. Dont worry we are on it. #awkard :/',
        'create.false.move_photo' => 'Ok, this is embarrassing, something weird happened when we tried to move your photo. Dont worry, we are on it ! :)',

        'updateUser.true' => 'Great stuff, you have successfully updated your account. :)',
        'updateUser.false.db_error' => 'Ok, this is embarrassing, something weird happened when we tried to update your account. Dont worry we are on it. #awkard :/',
        'updateUser.false.no_user' => 'Hey, this user account doesnt exist :/',
        'updateUser.false.no_user_id' => 'Hey, this is strange. You need to give us the user_id so we can update it. :)',

        'updatePassword.true' => 'Great stuff, you have successfully updated your password. :)',
        'updatePassword.false.db_error' => 'Ok, this is embarrassing, something weird happened when we tried to update your password. Dont worry we are on it. #awkard :/',
        'updatePassword.false.no_user' => 'Hey, this user account doesnt exist :/',
        'updatePassword.false.no_user_id' => 'Hey, this is strange. You need to give us the user_id so we can update it. :)',

        'updateAvatar.true' => 'Hey you have successfully changed your profile avatar. We just need to process it :)',
        'updateAvatar.false.no_file' => 'Ok, No file chosen. You need to choose the file to update your account',
        'updateAvatar.false.no_user' => 'Hey, this user account doesnt exist :/',
        'updateAvatar.false.move_file' => 'Ok, this is embarrassing, something weird happened when we tried to move your avatar. Dont worry, we are on it ! :)',
        'updateAvatar.false.no_user_id' => 'Ok, this is embarrassing, something weird happened, you didnt supply your user identification. #awkard :/',
    ];
    public function createUser()
    {

        $user = new User;
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->password = Hash::make(Input::get('password'));
        $user->email = Input::get('email');
        $user->status = 'Verified';
        $user->company = Input::get('company');
        $user->address = Input::get('address');
        $user->city = Input::get('city');
        $user->country = Input::get('country');
        $user->account_type = Input::get('account_type');
        $user->avatar = 'https://s3-eu-west-1.amazonaws.com/uniwink/cdn/images/photos/default-profile-photo.png';
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();



        if ($user->save()) {
            $user_id = $user->user_id;


            if (!empty($_FILES)) {

                $fileName = $_FILES['avatar']['name'];

                $tempPath = $_FILES['avatar']['tmp_name'];


                $uploadDirectory = 'C:\wamp\www\client_portal\app\storage' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $user_id;

                $old = umask(0);

                if (file_exists($uploadDirectory) == false) {
                    if (is_dir($uploadDirectory)) {
                        mkdir($uploadDirectory, 0775, true);
                    } else {
                        mkdir($uploadDirectory, 0775, true);
                    }
                }

                $uploadFilePath = $uploadDirectory . DIRECTORY_SEPARATOR . $fileName;

                $moveFile = move_uploaded_file($tempPath, $uploadFilePath);

                if ($moveFile) {


                    umask($old);
                    $avatar = $uploadFilePath;


                    // update user record
                    $user = User::find($user_id);
                    $oldfile =$user->avatar;
                    $user->avatar = $avatar;
                    $user->updated_at = Carbon::now();
                    $response = $user->forceSave();

                    if ($response === false) {
                        return $this->otherValidationFailureResponse($this->messages["createUser.false.photo_db_error"]);
                    }

                   // $this->removeLocalFiles($uploadDirectory);

                } else {
                    return $this->otherValidationFailureResponse($this->messages["createUser.false.move_photo"]);
                }

            }



            Event::fire('userCreated', array($user));
            return $this->recordCreatedResponse($this->messages["createUser.true"],$user->user_id,'true');

        } else {


            return $this->otherValidationFailureResponse($this->messages["createUser.false"]);
        }
    }


    public function removeLocalFiles($path)
    {

        try {

            if (is_dir($path) === true) {
                $files = array_diff(scandir($path), array('.', '..'));

                foreach ($files as $file) {
                    $this->removeLocalFiles(realpath($path) . '/' . $file);
                }

                rmdir($path);
            } else if (is_file($path) === true) {
                unlink($path);
            }

        } catch (Exception $ex) {
            Log::error($ex);
        }
    }


    public function updateUser($user_id)
    {
        if ($user_id) {
            $user = User::Find($user_id);
            if ($user) {

                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->email = Input::get('email');
                $user->status = 'Awaiting Verification';
                $user->company = Input::get('company');
                $user->address = Input::get('address');
                $user->city = Input::get('city');
                $user->country = Input::get('country');
                $user->account_type = Input::get('account_type');
                $user->updated_at = Carbon::now();
                $response = $user->forceSave();

                if ($response === true) {
                    //SegmentEventHandler::handleAccountUpdatedEvent($user);
                    return $this->recordUpdatedResponse($this->messages["updateUser.true"], $user_id, 'true');
                } else {
                    return $this->recordUpdatedResponse($this->messages["updateUser.false.db_error"], $user_id, 'false');
                }

            } else {
                return $this->otherValidationFailureResponse($this->messages["updateUser.false.no_user"]);
            }

        } else {
            return $this->otherValidationFailureResponse($this->messages["updateUser.false.no_user_id"]);
        }
    }


    public function updatePassword($user_id)
    {
        if ($user_id) {
            $user = User::Find($user_id);
            if ($user) {

                $user->password = Hash::make(Input::get('password'));
                $user->updated_at = Carbon::now();
                $response = $user->forceSave();

                if ($response === true) {
                    //SegmentEventHandler::handlePasswordUpdatedEvent($user);
                    return $this->recordUpdatedResponse($this->messages["updatePassword.true"], $user_id, 'true');
                } else {
                    return $this->recordUpdatedResponse($this->messages["updatePassword.false.db_error"], $user_id, 'false');
                }

            } else {
                return $this->otherValidationFailureResponse($this->messages["updatePassword.false.no_user"]);
            }

        } else {
            return $this->otherValidationFailureResponse($this->messages["updatePassword.false.no_user_id"]);
        }
    }

    public function updateAvatar($user_id)
    {

        if ($user_id) {

            if (!empty($_FILES)) {

                $fileName = $_FILES['avatar']['name'];

                $tempPath = $_FILES['avatar']['tmp_name'];


                $uploadDirectory = storage_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $user_id;

                $old = umask(0);

                if (file_exists($uploadDirectory) == false) {
                    if (is_dir($uploadDirectory)) {
                        mkdir($uploadDirectory, 0775, true);
                    } else {
                        mkdir($uploadDirectory, 0775, true);
                    }
                }

                $uploadFilePath = $uploadDirectory . DIRECTORY_SEPARATOR . $fileName;

                $moveFile = move_uploaded_file($tempPath, $uploadFilePath);

                if ($moveFile) {


                    umask($old);
                    $avatar = $uploadFilePath;


                    // update user record
                    $user = User::find($user_id);
                    $oldfile =$user->avatar;
                    $user->avatar = $avatar;
                    $user->updated_at = Carbon::now();
                    $response = $user->forceSave();

                    if ($response === true) {
                        // clean up files
                        $this->removeLocalFiles($oldfile); //delete old avatar
                        //SegmentEventHandler::handleAvatarUpdatedEvent($user);
                        return $this->recordUpdatedResponse($this->messages["updateAvatar.true"], $avatar, 'true');
                    } else {
                        return $this->recordUpdatedResponse($this->messages["updateAvatar.false.db_error"], $user_id, 'false');
                    }


                } else {
                    return $this->otherValidationFailureResponse($this->messages["updateAvatar.false.move_file"]);
                }

            }

        } else {
            return $this->otherValidationFailureResponse($this->messages["updateAvatar.false.no_user_id"]);
        }
    }

  /*  public function updateAvatar($user_id){
        if ($user_id) {
            $user = User::Find($user_id);
            if ($user) {

                $avatar = Input::file('avatar');
                $uploadDirectory = storage_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $user_id;

                if ($avatar) {

                    $old = umask(0);
                    $fileName = $avatar->getClientOriginalName();

                    if (file_exists($uploadDirectory) == false) {
                        if (is_dir($uploadDirectory)) {
                            mkdir($uploadDirectory, 0775, true);
                        } else {
                            mkdir($uploadDirectory, 0775, true);
                        }
                    }

                    $moveAvatar = $avatar->move($uploadDirectory, $fileName);

                    if ($moveAvatar) {
                        umask($old);


                        // update user record
                        $oldAvatar = $user->avatar;
                        $this->removeLocalFiles($oldAvatar);
                        $user->avatar = $avatar;

                        $user->updated_at = Carbon::now();
                        $response = $user->forceSave();

                        if ($response === true) {
                            return $this->recordUpdatedResponse($this->messages["updateAvatar.true"], $user_id, 'true');

                            //return $this->otherValidationFailureResponse($this->messages["createUser.false.photo_db_error"]);
                        }



                    } else {
                        return $this->otherValidationFailureResponse($this->messages["updateAvatar.false.move_file"]);
                    }

                }else {
                    return $this->otherValidationFailureResponse($this->messages["updateAvatar.false.no_file"]);

                }

         }

         else {
            return $this->otherValidationFailureResponse($this->messages["updateAvatar.false.no_user"]);
        }
        }else {
            return $this->otherValidationFailureResponse($this->messages["updateAvatar.false.no_user_id"]);
            }

    }*/
}



