<?php

use Illuminate\Http\Response as IlluminateResponse;

/**
 * Class ApiController
 */
class ApiController extends \BaseController {

    /**
     * @var int
     */
    private $statusCode = 200;
    /**
     * @var string
     */
    private $success = 'true';


	/*
	* @return mixed
	*/

    /**
     * @return int
     */
    public function getStatusCode(){
		return $this->statusCode;
	}

	/*
	* @param mixed $statusCode
	* @return $this
	*/

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode){
		$this->statusCode = $statusCode;
		return $this;
	}


	/*
	* @return mixed
	*/

    /**
     * @return string
     */
    public function getSuccess(){
		return $this->success;
	}

	/*
	* @param mixed $success
	* @return $this
	*/

    /**
     * @param $success
     * @return $this
     */
    public function setSuccess($success){
		$this->success = $success;
		return $this;
	}


    /**
     * @param string $message
     * @return mixed
     */
    public function dataNotFoundResponse($message = 'Not Found!'){
		return $this->setStatusCode(IlluminateResponse::HTTP_OK)->setSuccess('false')->respondWithMessage($message);
	}

    /**
     * @param string $message
     * @return mixed
     */
    public function noDataResponse($message = 'No Data!'){
		return $this->setStatusCode(IlluminateResponse::HTTP_OK)->setSuccess('false')->respondWithMessage($message);
	}

    /**
     * @param string $message
     * @return mixed
     */
    public function internalErrorResponse($message = 'Intenal Error!'){
		return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->setSuccess('false')->respondWithMessage($message);		
	}

    /**
     * @param $data
     * @return mixed
     */
    public function validationFailureResponse($data){
		return $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY)->setSuccess('false')->respondWithValidationDetail($data);
	}

    /**
     * @param string $message
     * @return mixed
     */
    public function otherValidationFailureResponse($message = 'Ooops, something failed!'){
        return $this->setStatusCode(IlluminateResponse::HTTP_OK)->setSuccess('false')->respondWithMessage($message);
    }

    /**
     * @param string $message
     * @param $id
     * @param $success
     * @return mixed
     */
    public function recordCreatedResponse($message = 'Successfull Created!', $id, $success,$secondId = null,$secondIdType = null){
		return $this->setStatusCode(IlluminateResponse::HTTP_OK)->setSuccess($success)->respondWithHybridMessage($message,$id,$secondId,$secondIdType);
	}

    /**
     * @param string $message
     * @param $id
     * @param $success
     * @return mixed
     */
    public function recordUpdatedResponse($message = 'Successfull Update!', $id,$success){
		return $this->setStatusCode(IlluminateResponse::HTTP_OK)->setSuccess($success)->respondWithHybridMessage($message,$id);		
	}

    /**
     * @param string $message
     * @param $id
     * @param $success
     * @return mixed
     */
    public function recordDeletedResponse($message = 'Successfull delete!',$id,$success){
		return $this->setStatusCode(IlluminateResponse::HTTP_OK)->setSuccess($success)->respondWithHybridMessage($message,$id);		
	}


    /**
     * @param string $message
     * @param $id
     * @param $success
     * @return mixed
     */
    public function recordAuxiliaryResponse($message = 'We have successfully completed your request!', $id, $success){
        return $this->setStatusCode(IlluminateResponse::HTTP_OK)->setSuccess($success)->respondWithHybridMessage($message,$id);
    }


    /**
     * @param $data
     * @return mixed
     */
    public function respondWithValidationDetail($data){
        return $this->respond([
            'success'=>$this->success,
            'validation'=>$data,
            'status_code'=>$this->statusCode
        ]);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function respondWithMessage($message){
		return $this->respond([
			'success'=>$this->success,
			'message'=>$message,
			'status_code'=>$this->statusCode
		]);
	}

    /**
     * @param $message
     * @param $id
     * @return mixed
     */
    public function respondWithHybridMessage($message,$id,$secondId= null,$secondIdType= null){

        if(isset($secondId)){
            return $this->respond([
                'success'=>$this->success,
                'message'=>$message,
                'status_code'=>$this->statusCode,
                'id'=>$id,
                $secondIdType=>$secondId,

            ]);
        }
        else{
            return $this->respond([
                'success'=>$this->success,
                'message'=>$message,
                'status_code'=>$this->statusCode,
                'id'=>$id,

            ]);
        }


	}


    /**
     * @param $object
     * @param $data
     * @return mixed
     */
    public function respondWithData($object,$data){
		return $this->respond([
			'success'=>$this->success,
			 $object=>$data,
			'status_code'=>$this->statusCode		
		]);
	}


    /**
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respond($data, $headers = []){
		return Response::json($data,$this->getStatusCode(),$headers);
	}





}
