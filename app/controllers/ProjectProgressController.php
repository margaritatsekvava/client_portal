<?php
class ProjectProgressController extends ApiController{

    private $messages = [

        'createProjectProgress.false.no_project' => 'Hey, this project doesnt exist :/',
        'createProjectProgress.false.no_project_id' => 'Hey, this is strange. You need to give us the project_id so we can update it. :)',
        'createProjectProgress.true' => 'Hey congrats, Project Progress is successfully created.)',
        'createProjectProgress.false' => 'Ok, this is embarrassing, something weird happened when we tried to create a Project Progress for you. Dont worry, we are on it ! :)',

        'ProjectCompleted.true' => 'Hey congrats, Project is successfully completed.)',
        'ProjectCompleted.false.no_project' => 'Hey, this project doesnt exist :/',
        'ProjectCompleted.false.no_project_id' => 'Hey, this is strange. You need to give us the project_id so we can update it. :)',
        'ProjectCompleted.false' => 'Ok, this is embarrassing, something weird happened when we tried to create a Project Progress for you. Dont worry, we are on it ! :)',
    ];


    public function createProjectProgress($project_id)
    {
        if ($project_id){

            $project = Project::find($project_id);

            if ($project){

                $progress = new ProjectProgress;
                $progress->project_id = $project_id;
                $progress->percentage = Input::get('percentage');
                $progress->comment = Input::get('comment');
                $progress->created_at = Carbon::now();
                $progress->updated_at = Carbon::now();

                if($progress->save()){

                    $user_id = $project->client_id;
                    $user=User::find($user_id);

                    if ($progress->percentage == 100){
                        Event::fire('projectCompleted', array($user,$project));
                    }
                    else{

                        Event::fire('progressCreated', array($user,$progress,$project ));

                        return $this->recordCreatedResponse($this->messages["createProjectProgress.true"],$progress->project_progress_id,'true');
                    }
                }

                else{
                    return $this->otherValidationFailureResponse($this->messages["createProjectProgress.false"]);

                }
            }
            else{
                return $this->otherValidationFailureResponse($this->messages["createProjectProgress.false.no_project"]);
            }
        }
        else{
            return $this->otherValidationFailureResponse($this->messages["createProjectProgress.false.no_project_id"]);

        }
    }


    public function completeProgress($project_id)
    {
        if ($project_id){

            $project = Project::find($project_id);


            if ($project){

                $progress = new ProjectProgress;
                $progress->project_id = $project_id;
                $progress->percentage = Input::get('percentage',100);
                $progress->comment = Input::get('comment','completed');
                $progress->created_at = Carbon::now();
                $progress->updated_at = Carbon::now();

                if($progress->save()){
                    $user_id = $project->client_id;

                    $user = User::find($user_id);

                    $project->status="Completed";
                        Event::fire('projectCompleted', array($user,$project));
                    //return $project;
                    return $this->recordCreatedResponse($this->messages["ProjectCompleted.true"],$progress->project_progress_id,'true');

                }

                else{
                    return $this->otherValidationFailureResponse($this->messages["ProjectCompleted.false"]);

                }
            }
            else{
                return $this->otherValidationFailureResponse($this->messages["ProjectCompleted.false.no_project"]);
            }
        }
        else{
            return $this->otherValidationFailureResponse($this->messages["ProjectCompleted.false.no_project_id"]);

        }
    }
}