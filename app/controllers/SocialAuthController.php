<?php


//use uniwink\Handlers\SegmentEventHandler;


/**
 * Class SocialAuthController
 */
class SocialAuthController extends ApiController {


    /**
     * @var array
     */
    private $messages = [
        'socialAuth.true'=>'Hey, thanks for logging in. :)',
        'socialAuth.false.socialProfile.db_error'=>'Ok, this is embarrassing, something weird happened when we tried to save your social profile. Dont worry, we are on it ! :)',

        'socialSyncFacebook.true'=>'Hey, thanks for syncing your facebook profile to your uniwink account. :)',
        'socialSyncFacebook.false.no_profile_identifier'=>'Ok, this is embarrassing, something weird happened when we tried to sync your social profile. There was no profile identifier ! :)',
        'socialSyncFacebook.false.no_profile'=>'Ok, this is embarrassing, something weird happened when we tried to sync your social profile. This social profile was not found :/',
        'socialSyncFacebook.false.no_user'=>'Ok, this is embarrassing, something weird happened when we tried to save your sync profile. This user was not found! :)',
        'socialSyncFacebook.false.db_error'=>'Ok, this is embarrassing, something weird happened when we tried to save your sync profile. But dont worry, we are on it ! :)',

        'socialSyncTwitter.true'=>'Hey, thanks for syncing your twitter profile to your uniwink account. :)',
        'socialSyncTwitter.false.no_profile_identifier'=>'Ok, this is embarrassing, something weird happened when we tried to sync your social profile. There was no profile identifier ! :)',
        'socialSyncTwitter.false.no_profile'=>'Ok, this is embarrassing, something weird happened when we tried to sync your social profile. This social profile was not found :/',
        'socialSyncTwitter.false.no_user'=>'Ok, this is embarrassing, something weird happened when we tried to save your sync profile. This user was not found! :)',
        'socialSyncTwitter.false.db_error'=>'Ok, this is embarrassing, something weird happened when we tried to save your sync profile. But dont worry, we are on it ! :)',

    ];


    public function socialAuth($socialPlatform, $auth = null){

        if($auth){
            try{
                Hybrid_Endpoint::process();
            }
            catch (Exception $e){
                return Redirect::to('')->with(Flash::error($e));
            }
            return;
        }

        $oauth = new Hybrid_Auth(app_path().'/config/hybridAuth.php');
        $provider = $oauth->authenticate($socialPlatform);
        $profile = $provider->getUserProfile();



        // Check if match
        switch ($socialPlatform) {
            case "Facebook":
                $socialProfile = FacebookProfile::checkIfProfileExists($profile->identifier);
                break;
            case "Twitter":
                $socialProfile = TwitterProfile::checkIfProfileExists($profile->identifier);
                break;
        }

        // if social profile match
        if($socialProfile == false){

            switch ($socialPlatform) {
                case 'Facebook':
                    $socialProfile = new FacebookProfile();
                    break;
                case 'Twitter':
                    $socialProfile = new TwitterProfile();
                    break;
            }

            // create new social profile and send to sync page
            $socialProfile->identifier=$profile->identifier;
            $socialProfile->website_url=$profile->webSiteURL;
            $socialProfile->profile_url=$profile->profileURL;
            $socialProfile->photo_url=$profile->photoURL;
            $socialProfile->display_name=$profile->displayName;
            $socialProfile->description=$profile->description;
            $socialProfile->first_name=$profile->firstName;
            $socialProfile->last_name=$profile->lastName;
            $socialProfile->gender=$profile->gender;
            $socialProfile->language=$profile->language;
            $socialProfile->age=$profile->age;
            $socialProfile->birth_day=$profile->birthDay;
            $socialProfile->birth_month=$profile->birthMonth;
            $socialProfile->birth_year=$profile->birthYear;
            $socialProfile->email=$profile->email;
            $socialProfile->email_verified=$profile->emailVerified;
            $socialProfile->phone=$profile->phone;
            $socialProfile->address=$profile->address;
            $socialProfile->country=$profile->country;
            $socialProfile->region=$profile->region;
            $socialProfile->city=$profile->city;
            $socialProfile->zip=$profile->zip;

            switch ($socialPlatform) {
                case 'Facebook':
                    $socialProfile->user_name=$profile->username;
                    $socialProfile->cover_info_url=$profile->coverInfoURL;
                    break;
            }

            $socialProfile->created_at=Carbon::now();
            $socialProfile->updated_at=Carbon::now();

            if ($socialProfile->save()) {

                switch ($socialPlatform) {
                    case 'Facebook':
                        return Redirect::action( 'PagesController@socialSyncFacebookView' )->with( 'social_profile', $socialProfile );
                        break;
                    case 'Twitter':
                        return Redirect::action( 'PagesController@socialSyncTwitterView' )->with( 'social_profile', $socialProfile );
                        break;
                }

            }
            else {
                return Redirect::to('')->with(Flash::error($this->messages["socialAuth.socialProfile.db_error"]));
            }

        }

        else{

            // check if user has this social profile
            switch ($socialPlatform) {
                case 'Facebook':
                    $user = User::where('facebook_profile_id','=',$socialProfile->facebook_profile_id)->first();
                    break;
                case 'Twitter':
                    $user = User::where('twitter_profile_id','=',$socialProfile->twitter_profile_id)->first();
                    break;
            }


            if($user){
                // if so login successfully
                switch ($socialPlatform) {
                    case 'Facebook':
                        SegmentEventHandler::handleFacebookLoginEvent($user->user_id);
                        break;
                    case 'Twitter':
                        SegmentEventHandler::handleTwitterLoginEvent($user->user_id);
                        break;
                }

                Auth::loginUsingId($user->user_id);

                $url = URL::route('app');
                return Redirect::to($url.Input::get('redirect'))->with(Flash::success($this->messages["socialAuth.true"]));
            }

            else{

                // if not redirect to social sync with social profile info
                switch ($socialPlatform) {
                    case 'Facebook':
                        return Redirect::action( 'PagesController@socialSyncFacebookView' )->with( 'social_profile', $socialProfile );
                        break;
                    case 'Twitter':
                        return Redirect::action( 'PagesController@socialSyncTwitterView' )->with( 'social_profile', $socialProfile );
                        break;
                }
            }


        }

    }


    public function socialSyncFacebook(){

        $profile_identifier  = Input::get('identifier');
        $email = Input::get('email');

            if($profile_identifier){

                $socialProfile = FacebookProfile::checkIfProfileExists($profile_identifier);

                if($socialProfile){

                    $user = User::where('email','=',$email)->first();

                    if($user){

                        $user->facebook_profile_id = $socialProfile->facebook_profile_id;
                        $user->updated_at = Carbon::now();
                        $response = $user->forceSave();

                        if ($response === true) {
                            SegmentEventHandler::handleFacebookProfileSyncEvent($user->user_id);
                            SegmentEventHandler::handleFacebookLoginEvent($user->user_id);
                            Auth::loginUsingId($user->user_id);
                            $url = URL::route('app');
                            return Redirect::to($url.Input::get('redirect'))->with(Flash::success($this->messages["socialAuth.true"]));
                        }

                        else {
                            return Redirect::to('')->with(Flash::error($this->messages["socialSyncFacebook.false.db_error"]));
                        }


                    }

                    else{
                        return Redirect::to('')->with(Flash::error($this->messages["socialSyncFacebook.false.no_user"]));
                    }

                }

                else{
                    return Redirect::to('')->with(Flash::error($this->messages["socialSyncFacebook.false.no_profile"]));
                }

            }

            else{
                return Redirect::to('')->with(Flash::error($this->messages["socialSyncFacebook.false.no_profile_identifier"]));
            }


    }

    public function socialSyncTwitter(){

        $profile_identifier  = Input::get('identifier');
        $email = Input::get('email');

        if($profile_identifier){

            $socialProfile = TwitterProfile::checkIfProfileExists($profile_identifier);

            if($socialProfile){

                $user = User::where('email','=',$email)->first();

                if($user){

                    $user->twitter_profile_id = $socialProfile->twitter_profile_id;
                    $user->updated_at = Carbon::now();
                    $response = $user->forceSave();

                    if ($response === true) {
                        SegmentEventHandler::handleTwitterProfileSyncEvent($user->user_id);
                        SegmentEventHandler::handleTwitterLoginEvent($user->user_id);
                        Auth::loginUsingId($user->user_id);
                        $url = URL::route('app');
                        return Redirect::to($url.Input::get('redirect'))->with(Flash::success($this->messages["socialAuth.true"]));
                    }

                    else {
                        return Redirect::to('')->with(Flash::error($this->messages["socialSyncTwitter.false.db_error"]));
                    }


                }

                else{
                    return Redirect::to('')->with(Flash::error($this->messages["socialSyncTwitter.false.no_user"]));
                }

            }

            else{
                return Redirect::to('')->with(Flash::error($this->messages["socialSyncTwitter.false.no_profile"]));
            }

        }

        else{
            return Redirect::to('')->with(Flash::error($this->messages["socialSyncTwitter.false.no_profile_identifier"]));
        }


    }



}