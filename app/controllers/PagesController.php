<?php



/**
 * Class PagesController
 */
class PagesController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexView()
    {
        return View::make('techtank.login.login');
    }

    public function loginView()
    {
        return View::make('techtank.login.login');
    }

    public function socialSyncFacebookView()
    {
        $social_profile = Session::get( 'social_profile' );
        if($social_profile){
            return View::make('techtank.social.syncFacebook')->with('social_profile', $social_profile);
        }
        else{
            return Redirect::to('')->with(Flash::error('An error has occurred with the social media sync, please try again :/'));
        }

    }

    public function socialSyncTwitterView()
    {
        $social_profile = Session::get( 'social_profile' );
        if($social_profile){
            return View::make('techtank.social.syncTwitter')->with('social_profile', $social_profile);
        }
        else{
            return Redirect::to('')->with(Flash::error('An error has occurred with the social media sync, please try again :/'));
        }

    }

}