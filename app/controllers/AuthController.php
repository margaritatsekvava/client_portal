<?php

//use uniwink\Handlers\SegmentEventHandler;

/**
 * Class AuthController
 */
class AuthController extends ApiController {


    /**
     * @var array
     */
    private $messages = [
        'login.true'=>'Welcome back!',
        'login.false'=>'Invalid user details',
        'login.false.account_not_enabled'=>'Hey, your account is not enabled. Please confirm your account by clicking on the link in the account confirmation email we sent you. :)',

        'login.false.subscription_over'=>'Hey, your subscription is now over. Please enter payment details to continue :)',
        'login.false.trial_over'=>'Hey, your trial is now over. Please enter payment details to continue :)',
        'logout.true'=>'Successfully logged out, come back soon!'
    ];

    public function login(){
        if(Auth::attempt(Input::only('email','password'))){

            if(Auth::user()->status !== 'Verified'){
                Auth::logout();
                return Redirect::to('')->with(Flash::success($this->messages["login.false.account_not_enabled"]));
            }

            else{

               // SegmentEventHandler::handleLoginEvent(Auth::user()->user_id);

                if(Input::get('redirect')){
                    $url = URL::route('app');
                    return Redirect::to($url.Input::get('redirect'))->with(Flash::success($this->messages["login.true"]));
                }
                else{
                    return Redirect::to('app')->with(Flash::success($this->messages["login.true"]));
                }
            }


        }else{
            return Redirect::to('')->with(Flash::success($this->messages["login.false"]));
        }
    }

    public function logout(){

       // if(Auth::user()){
           // SegmentEventHandler::handleLogoutEvent(Auth::user()->user_id);
        //}

        Auth::logout();
        return Redirect::to('')->with(Flash::success($this->messages["logout.true"]));
    }
}