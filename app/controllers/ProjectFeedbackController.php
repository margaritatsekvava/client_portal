<?php
class ProjectFeedbackController extends ApiController{

    private $messages = [

    'createProjectFeedback.false.no_project' => 'Hey, this project doesnt exist :/',
    'createProjectFeedback.false.no_project_id' => 'Hey, this is strange. You need to give us the project_id so we can update it. :)',
    'createProjectFeedback.true' => 'Hey congrats, Project Feedback is successfully created.)',
    'createProjectFeedback.false' => 'Ok, this is embarrassing, something weird happened when we tried to create a Project Feedback for you. Dont worry, we are on it ! :)',
];


    public function createProjectFeedback($project_id)
    {
        if ($project_id){

            $project = Project::find($project_id);

            if ($project){

                $feedback = new ProjectFeedback;
                $feedback->project_id = $project_id;
                $feedback->rating = Input::get('rating');
                $feedback->quotation = Input::get('quotation');
                $feedback->created_at = Carbon::now();
                $feedback->updated_at = Carbon::now();

                if($feedback->save()){
                    $admin_id=$project->admin_id;
                    $admin=User::find($admin_id);
                    $client_id = $project->client_id;
                    $user = User::find($client_id);

                    Event::fire('feedbackCreated', array($feedback,$project,$admin));
                    Event::fire('thankForFeedback', array($user, $project));

                    return $this->recordCreatedResponse($this->messages["createProjectFeedback.true"],$feedback->project_feedback__id,'true');
                   // return $feedback;
                }

                else{
                    return $this->otherValidationFailureResponse($this->messages["createProjectFeedback.false"]);

                }
            }
            else{
                return $this->otherValidationFailureResponse($this->messages["createProjectFeedback.false.no_project"]);
            }
        }
        else{
            return $this->otherValidationFailureResponse($this->messages["createProjectFeedback.false.no_project_id"]);

        }
    }


}