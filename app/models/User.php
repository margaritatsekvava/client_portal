<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;
use Laracasts\Presenter\PresentableTrait;
class User extends Ardent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, PresentableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
    protected $primaryKey = 'user_id';

    protected $presenter = 'UserPresenter';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public static $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'password' => 'required',
        'email' => 'required|email|unique:users',
        'password_confirmation' => 'same:password'
    );

    public static $customMessages = array(
        'first_name.required' => 'The attribute first name is required',
        'last_name.required' => 'The attribute last name is required',
        'password.required' => 'The attribute password is required',
        'email.required' => 'The attribute email is required',
        'email.unique' => 'Hey, this email is already in use by someone cool. Is it you? :P',
        //'user_name.required' => 'The attribute user_name is required',
        //'user_name.unique' => 'Hey, this user_name is already in use by someone cool. Unfortunately, its not you :P',
    );


	//protected $hidden = array('password', 'remember_token');

    protected $fillable = array('first_name', 'last_name', 'email','password');

    protected $guarded = array('password');


    public function projects(){

        return $this->hasMany('Project','user_id');
    }

    public function facebookProfile(){

        return $this->hasOne('FacebookProfile', 'user_id');
    }

    public function twitterProfile(){

        return $this->hasOne('twitterProfile', 'user_id');
    }


}
