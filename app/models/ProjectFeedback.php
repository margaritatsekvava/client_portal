<?php
use LaravelBook\Ardent\Ardent;

class ProjectFeedback extends Ardent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_feedback';
    protected $primaryKey = 'project_feedback__id';

    public static $rules = array(
        'rating' => 'required',
       // 'project_id'=>'project_id',


    );

    public static $customMessages = array(
        'rating.required' => 'The field percentage is required',
        //'project_id.required'=> 'The field project_id is required',

    );




    protected $fillable = array('rating','quotation', 'project_id');
    protected $guarded = array('note_feedback_id');


    public function project(){

        return $this->belongTo('Project', 'project_id');
    }

}