<?php
use LaravelBook\Ardent\Ardent;


class FacebookProfile extends Ardent{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facebook_profiles';
    protected $primaryKey='facebook_profile_id';




    public static function checkIfProfileExists($identifier){

        $profile = FacebookProfile::where('identifier','=',$identifier)->first();

        if($profile){
            return $profile;
        }

        else{
            return false;
        }
    }

    public function user(){
        return $this->belongsTo('User','user_id');
    }

}