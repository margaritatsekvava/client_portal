<?php
use LaravelBook\Ardent\Ardent;

class ProjectProgress extends Ardent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_progress';
    protected $primaryKey = 'project_progress_id';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static $rules = array(
        'percentage' => 'required',
        //'project_id'=>'project_id',


    );

    public static $customMessages = array(
        'percentage.required' => 'The field percentage is required',
        //'project_id.required'=> 'The field project_id is required',

    );


    protected $fillable = array('percentage', 'comment');

    protected $guarded = array('project_progress_id');


    public function project(){

        return $this->belongsTo('Project', 'project_id');
    }


}