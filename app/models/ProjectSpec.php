<?php
use LaravelBook\Ardent\Ardent;

class ProjectSpec extends Ardent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_specs';
    protected $primaryKey = 'project_spec_id';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static $rules = array(
        'name' => 'required',
        'location'=>'required',


    );

    public static $customMessages = array(
        //'name.required' => 'The name for project spec is required',
        //'location.required'=>'The field location is required',

    );

    protected $fillable = array('name', 'location', 'status','thumbnail');

    protected $guarded = array('project_spec_id');


    public function project(){
        return $this->belongsTo('Project','project_id');
    }
}