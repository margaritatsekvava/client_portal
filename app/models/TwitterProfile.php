<?php


use LaravelBook\Ardent\Ardent;

class TwitterProfile extends Ardent{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'twitter_profiles';
    protected $primaryKey='twitter_profile_id';




    public static function checkIfProfileExists($identifier){

        $profile = TwitterProfile::where('identifier','=',$identifier)->first();

        if($profile){
            return $profile;
        }

        else{
            return false;
        }
    }

    public function user(){
        return $this->belongsTo('User','user_id');
    }
}