<?php
use LaravelBook\Ardent\Ardent;

class Project extends Ardent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';
    protected $primaryKey = 'project_id';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static $rules = array(
        'name' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',

    );

    public static $customMessages = array(
        'name.required' => 'The project name is required',
        'start_date.required' => 'The attribute start_date name is required',
        'end_date.required' => 'The attribute end_date is required',

    );

    protected $fillable = array('name', 'description', 'start_date','end_date','client_id', 'admin_id');

    protected $guarded = array('project_id');


    public function progress(){

        return $this->hasMany('ProjectProgress', 'project_id');
    }



    public function latestProgress(){

        return $this->hasMany('ProjectProgress', 'project_id')->orderBy('percentage','desc')->take(1);
    }

    public function feedback(){

        return $this->hasMany('ProjectFeedback', 'project_id');
    }

    public function client(){

        return $this->belongsTo('User', 'client_id');
    }

    public function admin(){
        return $this->belongsTo('User', 'admin_id');

    }


    public function spec(){
        return $this->hasOne('ProjectSpec','project_id');
    }
}
